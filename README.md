# PlutusPrerequisites



Contains sample for react query , native base and react native with typescript


Resources Links :

# React Query 

https://www.youtube.com/playlist?list=PLC3y8-rFHvwjTELCrPrcZlo6blLBUspd2

https://www.section.io/engineering-education/react-query-data-fetching-and-server-state-management/#:~:text=Benefits%20of%20using%20React%2DQuery%20in%20react.&text=We%20can%20set%20the%20number,the%20request%20operation%20is%20optimized.

# Native Base

https://docs.nativebase.io/

