import {NavigationContainer} from '@react-navigation/native';
import {NativeBaseProvider} from 'native-base';
import * as React from 'react';
import {FC} from 'react';
import {StyleSheet} from 'react-native';
import {QueryClient, QueryClientProvider} from 'react-query';
import Main from './Navigation/Main';
import CustomIcons from './Screens/CustomIcons';

interface App {
  title: string;
}

const queryClient = new QueryClient();

const App: FC<App> = props => {
  return (
    <NavigationContainer>
      <QueryClientProvider client={queryClient}>
        <NativeBaseProvider>
          <Main />
          {/* <CustomIcons/> */}
        </NativeBaseProvider>
      </QueryClientProvider>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
  textstyle: {
    textAlign: 'center',
    fontSize: 18,
  },
});

export default App;
