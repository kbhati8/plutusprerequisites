import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Posts from '../Screens/Posts';
import Post from '../Screens/Post';
import {FC} from 'react';

const MainStack = createStackNavigator();

const Main: FC = () => {
  return (
    <MainStack.Navigator initialRouteName="Home">
      <MainStack.Screen name="Home" component={Posts} />
      <MainStack.Screen name="Post" component={Post} />
    </MainStack.Navigator>
  );
};

export default Main;
