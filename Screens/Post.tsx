import {FC, useState} from 'react';
import * as React from 'react';
import {Heading, Text, ScrollView, Box, Spinner} from 'native-base';
import {width} from '../Utils/Responsive';
import usePost from '../Hooks/usePost';
import usePosts from '../Hooks/usePosts';

interface Post {}

const Post: FC = (route: any) => {
  const {post} = route.route.params;
  const {data: comments, isSuccess, isLoading} = usePost(post.id);
  return (
    <ScrollView flex="1" minW={width}>
      <Heading textAlign="center" p="5">
        <Text fontWeight="extrabold" fontSize="3xl">
          {post.title}
        </Text>
      </Heading>
      <Box p="5" alignItems="center">
        <Text fontWeight="normal" fontSize="xl">
          {post.body}
        </Text>
      </Box>
      <Heading textAlign="center" p="2">
        <Text fontWeight="extrabold" fontSize="3xl">
          Comments
        </Text>
      </Heading>
      <Box p="5">
        {isLoading && <Spinner />}
        {isSuccess && (
          <>
            {comments.map((data: any, index: number) => {
              return (
                <Box m="2" key={index}>
                  <Text fontWeight="normal" fontSize="xl">
                    {data.body}
                  </Text>
                  <Text fontWeight="normal" fontSize="xl">
                    {data.email}
                  </Text>
                </Box>
              );
            })}
          </>
        )}
      </Box>
    </ScrollView>
  );
};

export default Post;
