import {FC, useEffect} from 'react';
import * as React from 'react';
import {
  Pressable,
  Text,
  Box,
  Container,
  Spinner,
  FlatList,
  ScrollView,
} from 'native-base';
import {width} from '../Utils/Responsive';
import usePosts from '../Hooks/usePosts';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

interface Posts {}

export type RootStackParamList = {
  Post: {post: listData};
};

interface listData {
  body: string;
  title: string;
  id: number;
  userId: number;
}

const renderItem = (item: listData, navigation: any) => {
  return (
    <Pressable
      onPress={() => {
        navigation.navigate('Post', {post: item});
      }}>
      <Box
        p="5"
        m="2"
        minW={width - 10}
        rounded="lg"
        alignSelf="flex-start"
        bg="gray.800"
        _text={{
          fontSize: 'md',
          fontWeight: 'medium',
          color: 'warmGray.200',
          letterSpacing: 'lg',
        }}>
        <Text fontSize="sm" color="coolGray.200">
          {item.title}
        </Text>
      </Box>
    </Pressable>
  );
};

const Posts: FC = () => {
  const {data, isLoading, isSuccess} = usePosts();
  const navigation = useNavigation<StackNavigationProp<RootStackParamList>>();

  return (
    <ScrollView flex="1" minW={width}>
      {isLoading && <Spinner />}
      {isSuccess && (
        <>
          <Text fontSize="3xl" textAlign="center">All Posts</Text>
          <FlatList
            data={data}
            renderItem={({item}: {item: listData}) => {
              return renderItem(item, navigation);
            }}
            keyExtractor={item => String(item.id)}
          />
        </>
      )}
    </ScrollView>
  );
};

export default Posts;
