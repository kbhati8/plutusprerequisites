import {useQuery} from 'react-query';
import axios from 'axios';

const fetchPost = async (postId: number) => {
  const {data} = await axios.get(
    `https://jsonplaceholder.typicode.com/comments?postId=${postId}`,
  );
  console.log('fired');
  return data;
};

const usePost = (postId: number) =>
  useQuery(['posts', postId], () => fetchPost(postId), {staleTime: 5000});
export default usePost;
